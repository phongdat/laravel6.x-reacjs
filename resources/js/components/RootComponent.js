import React, {Component} from 'react';

class RootComponent extends Component{
    render() {
        return (
            <div className="container">
                RootComponent!
            </div>
        )
    }
}

export default RootComponent;